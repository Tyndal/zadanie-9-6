var add = document.getElementById("js-addElem");
var list = document.getElementById("js-list");
var listElements = document.getElementsByTagName("li");

function addList() {
  var listLength = listElements.length;
  list.innerHTML += "<li>item " + listLength + "</li>";
}

add.addEventListener("click", addList);
